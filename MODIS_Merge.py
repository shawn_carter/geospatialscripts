# -*- coding: utf-8 -*-
"""
MODIS Global Dataset Mosaiking
Created on Sun Mar  8 11:39:24 2015

@author: shawn m carter

This script converts MODIS 10x10 HDF Tiles into GeoTiffs, merges, then 
reprojects them
"""
import os

# Change the variable of the working directory to where the hdf files are 
# located



# Create a list of MODIS HDF Tiles

def hdf2tif(fileLocation, hdf_band):
    os.chdir(MODIS)
    gdal_pre = 'gdal_translate -of GTiff HDF4_EOS:EOS_GRID:'
    hdfs = []
    files = os.listdir(fileLocation)
    for f in files:
        if f.endswith('.hdf'):
            hdfs.append(f)
            
    for hdf in hdfs:
        gdal_post = hdf +':MOD12Q1:' + hdf_band
        newName= hdf.strip('.hdf') + '.tif'
        expr = gdal_pre + gdal_post + ' ' + newName
        os.system(expr)

def tifMerge(merge_name):
    tifs = []
    files = os.listdir('/home/shawn/Documents/MODIS/2012')
    for f in files:
        if f.endswith('.tif'):
            tifs.append(f)
    
    expr1 = 'gdal_merge.py '+' '.join(tifs)
    expr2 = ' -o ' + merge_name
    expr = expr1 + expr2
    os.system(expr)
    
def tifWarp(merge_name, warp_name):
    expr1 = 'gdalwarp -t_srs "EPSG:4326" ' + merge_name + ' ' + warp_name
    os.system(expr1)
    
MODIS = raw_input('Provide the full directory where your MODIS HDF files are located:  ')
hdf_band = raw_input('Input the band name that you wish to extract from the MODIS file (i.e. Land_Cover_Type_5). \nThis info can be found by running gdalinfo /filename/ and looking up the subdatasets.: ')
merge_name = raw_input('Merged but unprojected .tif name: ')
warp_name = raw_input('Final projected filename: ')
    
hdf2tif(MODIS, hdf_band)

tifMerge(merge_name)

tifWarp(merge_name,warp_name)




